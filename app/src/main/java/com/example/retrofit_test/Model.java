package com.example.retrofit_test;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model {
    private String name;
    private String image;

    public ArrayList<String> getItems() {
        return items;
    }

    private ArrayList<String> items;

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}
