package com.example.retrofit_test;

import java.util.ArrayList;

public class Response {

    private String status;
    private String message;

    public Data data;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }
}
