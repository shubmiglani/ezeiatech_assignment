package com.example.retrofit_test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<Model> models ;
    Adapter obj;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = findViewById(R.id.List);

        obj = new Adapter(MainActivity.this, R.layout.recycle_layout, models);

        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerView.setAdapter(obj);


        Call<Response> makeCall = RetrofitRequest.getitem();
        makeCall.enqueue(new Callback<Response>() {

            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response responseBody = response.body();
                Log.e("MainAct ","response ::: " );
                models = responseBody.getData().users;
                obj.setA1(models);
                obj.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.e("MainAct", t.getLocalizedMessage());
            }
        });
    }

}


