package com.example.retrofit_test;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RequestInterface {

    @GET("users")
    Call<Response> item(
            @Query("offset")int ofset,
            @Query("limit")int limit);


}
