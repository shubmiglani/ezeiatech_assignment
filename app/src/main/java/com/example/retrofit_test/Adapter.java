package com.example.retrofit_test;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.PostViewHolder> {

    private Activity obj;
    private int res;
    private List<Model> A1;


    public Adapter(Activity obj, int res, List<Model> A1) {
        this.obj = obj;
        this.res = res;
        this.A1 = A1;
    }


    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = (View) inflater.inflate(R.layout.recycle_layout, viewGroup, false);
        return new PostViewHolder(view);
    }

    public void setA1(List<Model> a1) {
        A1 = a1;
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder postViewHolder, int i) {

        Model s = A1.get(i);


        Glide.with(postViewHolder.imageView.getContext())
                .load(s.getImage())
                .into(postViewHolder.imageView);

        postViewHolder.textView.setText(s.getName());

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayout.VERTICAL);
        postViewHolder.recyclerView.setLayoutManager(staggeredGridLayoutManager);
        StaggeredAdapter adapter;
        if (s.getItems().size() % 2 == 0) {
            adapter = new StaggeredAdapter(obj, s.getItems());
        } else {
            String oddImageString = s.getItems().get(0);
            Glide.with(postViewHolder.oddImageView.getContext())
                    .load(oddImageString)
                    .into(postViewHolder.oddImageView);

            s.getItems().remove(0);
            adapter = new StaggeredAdapter(obj, s.getItems());
        }

        postViewHolder.recyclerView.setAdapter(adapter);


    }

    @Override
    public int getItemCount() {
        return A1 == null ? 0 : A1.size();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        RecyclerView recyclerView;
        ImageView oddImageView;


        @SuppressLint("ResourceType")
        public PostViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image);
            textView = (TextView) itemView.findViewById(R.id.text);

            recyclerView = itemView.findViewById(R.id.rcvItems);
            oddImageView = itemView.findViewById(R.id.oddImage);
        }
    }

}
