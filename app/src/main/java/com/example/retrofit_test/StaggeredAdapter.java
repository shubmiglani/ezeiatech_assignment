package com.example.retrofit_test;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class StaggeredAdapter extends RecyclerView.Adapter<StaggeredAdapter.StaggeredHolder>{

    private List<String> A1;
    private Activity obj;

    public StaggeredAdapter(Activity obj,  List<String> A1) {
        this.obj = obj;
        this.A1 = A1;
    }


    @NonNull
    @Override
    public StaggeredAdapter.StaggeredHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = (View) inflater.inflate(R.layout.new_recycle_layout, parent, false);
        return new StaggeredAdapter.StaggeredHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StaggeredAdapter.StaggeredHolder holder, int position) {
        String s = A1.get(position);
        Glide.with(holder.imageView.getContext())
                .load(s)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return A1 == null?0:A1.size();
    }

    public class StaggeredHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public StaggeredHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
        }
    }
}
